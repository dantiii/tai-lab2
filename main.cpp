#include <iostream>

int main() {
    std::cout << "Hello, World!" << std::endl;
    return 0;
}
// https://stackoverflow.com/questions/13660777/c-reading-the-data-part-of-a-wav-file
/*
 *  ReadRIFF();
    ReadFMT();
    int32 chunk2Id = Read32(BigEndian);
    int32 chunk2Size = Read32(LittleEndian);
    for (int i = 0; i < chunk2Size; i++)
    {
        audioData[i] = ReadByte();
    }
 */